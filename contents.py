import pathlib

file_l = []


def file_and_dir(folder1):
    for file_or_dir in folder1.iterdir():
        if file_or_dir.is_file() and str(file_or_dir).endswith(".txt"):
            file_l.append(str(file_or_dir))
        elif file_or_dir.is_dir():
            file_and_dir(pathlib.Path(file_or_dir))
    return file_l


folder = pathlib.Path('.')
text_l = []
for file in file_and_dir(folder):
    with open(file, 'r') as file_input:
        text_l.append(file_input.read())

with open("contents.txt", 'a') as contents_input:
    for text in sorted(text_l):
        contents_input.write(text)
